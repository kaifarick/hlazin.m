﻿using UnityEngine;
using UnityEngine.SceneManagement;
namespace Zenject.SpaceFighter
{

    public class GameManager : MonoBehaviour
    {

        public GameObject DeathScreen;
 
        public void Update()
        {
            Death();
        }

        public void Death()
        {
            if (Player._health <= 0)
            {
                DeathScreen.SetActive(true);
                Time.timeScale = 0f;
            }
        }

        public void Continue()
        {
            Player._health = 100;
            DeathScreen.SetActive(false);
            Time.timeScale = 1f; 

        }

        public void RestartBtn()
        {
            Player._health = 100;
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
